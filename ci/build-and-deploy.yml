# build-and-deploy-document.yml
# Build and deploy a pdf and html document from the standard template
#
# dependencies docker-runner
#
# Needs DOCNAME set as a global variable
#
# usage:
#
# include:
#    project: 'standards-process/gitlab-ci-pipelines'
#    file: '/build-and-deploy-document.yml'
#
# stages:
#   - build
#   - deploy
#

# --------------------------------------------------------------------------------

.document:
  variables:
    TOG_DOCNAME: ${DOCNAME}
  stage: build
  tags: ['docker-runner']
  artifacts:
    paths:
      - public
      - job-urls
    expire_in: 1 day
  before_script:
    - gem install asciidoctor-reducer
    - asciidoctor --version
#    - env
    - mkdir -p public
    - mkdir -p job-urls
    - mkdir -p public/antora
    - mkdir -p public/main  # link to pdf for antora
    - echo $CI_JOB_URL > "job-urls/$CI_JOB_NAME.txt"

html -- build:
  extends: .document
  script:
    - bin/adocbuild.sh html
    - find output -print
    - cp -r output/* public/

pdf -- build:
  extends: .document
  script:
    - bin/adocbuild.sh pdf
    - find output -print
    - cp -r output/* public/

epub -- build:
  extends: .document
  script:
    - bin/adocbuild.sh epub
    - find output -print
    - cp -r output/* public/

antora -- build:
  extends: .document
  script:
    - |
      if [ -z "$GIT_CREDENTIALS" ]; then
          echo "GIT_CREDENTIALS variable is not set. Skipping the build."
      else
          antora --fetch --to-dir=public/antora antora-playbook.yml
      fi

package source:
  stage: build
  image: alpine
  tags: ['docker-runner']
  artifacts:
    paths:
      - public
      - job-urls
    expire_in: 1 day
  before_script:
    - apk add git zip
    - mkdir -p public
    - mkdir -p job-urls
    - echo $CI_JOB_URL > "job-urls/$CI_JOB_NAME.txt"
  script:
    - git ls-tree -r HEAD --name-only | zip "public/$ZIPFILE" -@

# --------------------------------------------------------------------------------

pages:
  stage: deploy
  tags: ['docker-runner']
  variables:
    TOG_DOCNAME: ${DOCNAME}
  artifacts:
    paths:
    - public
    expire_in: 1 day
# deprecated
#  only:
#     - master
  rules:
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    # Make a simple index.html file to sit at the top level of the Pages output
    # We can move this to a separate file in the source tree at some point
    - |
      DTSTAMP=`date`
      HTML_LOG=`cat "job-urls/html -- build.txt"`
      PDF_LOG=`cat "job-urls/pdf -- build.txt"`
      EPUB_LOG=`cat "job-urls/epub -- build.txt"`
      PACKAGE_LOG=`cat "job-urls/package source.txt"`
      cat >public/index.html <<!EOF
      <html>
      <title>Build Output</title>
      <body>
      <h1>Build Output</h1>
      <h2>Draft Document</h2>
      <ul>
      <li>
      <a href=html/${TOG_DOCNAME}.html>HTML</a></li>
      <li>
      <a href=pdf/${TOG_DOCNAME}.pdf>PDF</a> </li>
      <li>
      <a href=epub/${TOG_DOCNAME}.epub>EPUB</a> </li>
      <li>
      <a href="${ZIPFILE}">Full Source (ZIP)</a> </li>
      </ul>
      <p>
      <a href="${HTML_LOG}">HTML Build log output</a> |
      <a href="${PDF_LOG}">PDF Build log output</a> |
      <a href="${PACKAGE_LOG}">Package log output</a> |
      <a href="${EPUB_LOG}">EPUB Build log output</a>
      !EOF
    - |
      if [ -z "$GIT_CREDENTIALS" ]; then
          cat >>public/index.html <<!EOF
          <p>Antora build skipped.</p>
      !EOF
      else
          cd public/main && ln -s ../pdf/${TOG_DOCNAME}.pdf ${CI_PROJECT_NAME}.pdf
          cd ../..
          cat >>public/index.html <<!EOF
          <ul>
          <li>
          <a href="antora/index.html">Antora Output</a> </li>
          </ul>
      !EOF
      fi
    - |
      cat >>public/index.html <<!EOF
      <p>
      $DTSTAMP
      </body>
      </html>
      !EOF
