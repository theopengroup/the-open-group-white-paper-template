#helper script to prepare a single html file for the document review system
# This tidies a single html file 

# tunehtml is a small c program to remove the toc from the left margin
#  It removes the lines from
#    <div id="toc" class="toc2">
# up to but not including
#    <div id="content">
# source included at the end of the file



# Adjust the <body> class to get rid of the left margin
# Adjust lists and tables to remove embedded paragraphs within elements <li> and <dd>
echo "Removing left margin and tidying lists and paragraphs"
for i in $*
do
tunehtml $i | sed -e 's/class="book toc2 toc-left"/class="book"/g' |  perl -p -000 -e 's/<li>\s+<p>/<li>/g' |perl -p -000 -e 's/<\/p>\s+<\/li>/<\/li>/g'  | perl -p -000 -e 's/<dd>\s+<p>/<dd>/g' |perl -p -000 -e 's/<\/p>\s+<\/dd>/<\/dd>/g' | sed -e's/<p class="tableblock">//g' -e 's/<\/p><\/td>/<\/td>/g' >.$i

mv .$i $i
done

# remove the footnotes as would need a manual fixup after generating as multipages output
# remove an embedded character in a quote which is not displayed correctly in the document review system

# fixup references , multipass as there can be up to 8 references on a single html line
for i in $*
do
sed '/<div id="footnotes">/,/<div id="footer">/d' $i |  sed -e's/<sup class="footnote">\(.*\)<\/sup>//g' |  sed  -e 's/quoteblock blockquote::before{content:".201c"/quoteblock blockquote::before{content:""/g' |  sed -e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g' \
-e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g' \
-e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g' \
-e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g' \
-e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g' \
-e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g' \
-e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g' \
-e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g' \
-e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g' \
-e 's/\[<a href="#\([A-Za-z0-9].*\)">\([0-9]\)/\[<a href="Referenced-docs.html#\1">\2/g'  \
-e 's/, <a href="#\([A-Za-z0-9].*\)">\([0-9]\)/, <a href="Referenced-docs.html#\1">\2/g' \
-e 's/, <a href="#\([A-Za-z0-9].*\)">\([0-9]\)/, <a href="Referenced-docs.html#\1">\2/g' >.$i


mv .$i $i
done

# delink self references, so just to the anchor and not cause a reload

for i in $*
do
echo $i
NAME=$i
 PATT="s/<a href=\"$NAME#\(.*.\)/<a href=\"#\1/g"

sed -e "$PATT" $i >.$i
mv .$i $i

done

exit 0

##include <stdio.h>
##include <string.h>
#main (argc, argv)
#int argc;
#char *argv[];
#{
#FILE *fp;
#char buf[512];
#int i;
#
#fp = fopen(argv[1], "r"); /* open the named filed */
#
#i=0;
#while (fgets(buf, sizeof buf, fp) != NULL) {
#        if (strncmp (buf, "<div id=\"toc\" class=\"toc2\">", 27)== 0) {
#                i=1;
#         } else if (strncmp(buf, "<div id=\"content\">", 18) ==0 ) {
#                i=0;
#                }
#        if (i == 0 ) {
#                fputs (buf, stdout);
#        }
#    }
#}
