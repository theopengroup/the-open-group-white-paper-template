#!/bin/bash
# simple HTML build - independent

if [ ! -r bin/sedscript_file ]
then
    echo "$0: prerequisite bin/sedscript_file not found"
    exit 1
fi

# setup images at top level for inclusion in builds
rm -rf images
bin/image-cache.sh

TOG_DOCNAME=$1
#asciidoctor -v -r asciidoctor-bibtex -D output/html -o ${TOG_DOCNAME}.html  _book-html.adoc

echo "Preprocess"
# https://github.com/asciidoctor/asciidoctor-reducer
asciidoctor-reducer _book-html.adoc >output/preproc.adoc
echo "Adjust source"
cat output/preproc.adoc |sed -f bin/sedscript_file >_pout.adoc
echo "Processing html"
asciidoctor -v -r asciidoctor-diagram -D output/html -o ${TOG_DOCNAME}.html _pout.adoc
echo "Processing complete"

# remove images
rm -rf images
