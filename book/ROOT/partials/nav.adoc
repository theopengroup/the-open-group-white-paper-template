.The Open Group White Paper Template
* Front Matter
** xref:00-front-matter:title-page.adoc[Title]
** xref:00-front-matter:copyright.adoc[Copyright]
* xref:00-front-matter:exec-summary.adoc[Executive Summary]
* xref:01-doc:chap01.adoc[Introduction]
* xref:99-back-matter:references.adoc[References]
* xref:99-back-matter:about-the-author.adoc[About the Authors]
* xref:99-back-matter:about-the-open-group.adoc[About The Open Group]
