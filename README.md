# The Open Group White Paper Template

A template for developing White Papers for The Open Group written in asciidoc using the asciidoctor toolchain. 
This version is configured to auto build the html and pdf output on The Open Group Gitlab server. It can also be built locally which is useful for development purposes. It is also suitable for use as an Antora module.

*Please note that users are expected to be proficient in the use of gitlab and asciidoctor.
This is not intended as a tutorial for configuring git clients, nor installing the toolchain*
.

If you are looking to build a standard, a snapshot of a standard, or a guide, please use the specific template,

You can view the latest build of this template http://standards-process.projects.opengroup.org/the-open-group-white-paper-template

The file tree is organized as follows:

*  bin (directory) - scripts to build the document in a number of formats
*  resources (directory) - style sheets for building pdf
*  modules (directory) - the actual document text
*  output (directory) - where output files are created

In general files beginning underscore (\_) are configuration files. They may need to be tailored for the document.

Configuration files:


*  \_strings.adoc - document strings to configure the document
*  \_files.adoc - the table of contents for the document layout
*  \_vars.sh - variables in shell script syntax to configure the build scripts
*  \_book-html.adoc - configuration for the html book output
*  \_book-pdf.adoc - configuration for the pdf book output
* antora.yml - configuration file for Antora module output


Working with the template

* Review \_strings.adoc and edit the variables.

* Building the document locally using bin/adocbuild.sh or autobuild on the server

The builds can be run using the automated pipeline build or run locally on the client using scripts in the build directory.

To build the document locally, use the script in the _bin_ Directory:

  $ bin/adocbuild.sh

The options are:

  bin/adocbuild.sh [clean | html | pdf | all]

Ruby Prerequisites: asciidoctor, asciidoctor-bibtex, asciidoctor-pdf, unicode_utils

To understand the template its best to build the pdf document, and read it.
