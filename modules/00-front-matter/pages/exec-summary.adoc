// blank

= Executive Summary

This should be a short paragraph that describes the content of the document, listing the top three to five key points covered. It should state to whom this document will be of interest and why it is relevant to the target audience.

A second, short paragraph should explain how the material covered in this document supports The Open Group vision: Boundaryless Information Flow.
